//
//  MyCoreDataStack.m
//  C1ients
//
//  Created by Dmitry Shmidt on 19/09/14.
//  Copyright (c) 2014 Dmitry Shmidt. All rights reserved.
//

#import "CloudKitCoreDataStack.h"
#import <CoreData/CoreData.h>
@interface CloudKitCoreDataStack ()
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@end

@implementation CloudKitCoreDataStack
@synthesize managedObjectModel = _managedObjectModel;
- (NSString *)managedObjectModelName
{
    return @"Recipes";
}
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}
@end
