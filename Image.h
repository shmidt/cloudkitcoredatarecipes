//
//  Image.h
//  Recipes
//
//  Created by Dmitry Shmidt on 26/09/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AbstractCloudRecord.h"

@class Recipe;

@interface Image : AbstractCloudRecord

@property (nonatomic, retain) id image;
@property (nonatomic, retain) Recipe *recipe;

@end
