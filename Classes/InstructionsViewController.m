/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract: View controller to manage a text view to allow the user to edit instructions for a recipe.
 
  Version: 1.5
*/

#import "InstructionsViewController.h"
#import "Recipe.h"

@interface InstructionsViewController ()

@property (nonatomic, strong) IBOutlet UITextView *instructionsText;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;

@end


#pragma mark -

@implementation InstructionsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationItem.title = @"Instructions";
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Update the views appropriately
    self.nameLabel.text = self.recipe.name;
    self.instructionsText.text = self.recipe.instructions;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
NSLog(@"%s",__PRETTY_FUNCTION__);
    [super setEditing:editing animated:animated];

    self.instructionsText.editable = editing;
	[self.navigationItem setHidesBackButton:editing animated:YES];

	/*
	 If editing is finished, update the recipe's instructions and save the managed object context.
	 */
	if (!editing) {
		self.recipe.instructions = self.instructionsText.text;
		[self.recipe saveWithCompletionHandler:^(id managedObject, NSError *error) {
            NSLog(@"Updating %@", managedObject);
            if (error) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }];
	}		
}

@end
