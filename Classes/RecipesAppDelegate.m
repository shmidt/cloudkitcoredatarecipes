/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract: Application delegate that sets up a tab bar controller with two view controllers -- a navigation controller that in turn loads a table view controller to manage a list of recipes, and a unit converter view controller.
 
  Version: 1.5 
 */

#import "RecipesAppDelegate.h"
#import "RecipeListTableViewController.h"
#import "Recipe.h"

#import <CloudKit/CloudKit.h>
#import "CloudKitCoreDataStack.h"
#import "CBROfflineCapableCloudConnection.h"
//#import <FXReachability.h>
//#import <Reachability.h>
@interface RecipesAppDelegate ()

@end


#pragma mark -

@implementation RecipesAppDelegate{
    CBROfflineCapableCloudBridge *_cloudBridge;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    CKDatabase *database = [CKContainer defaultContainer].publicCloudDatabase;
    CloudKitCoreDataStack *stack = [CloudKitCoreDataStack sharedInstance];
    CBRCloudKitConnection *connection = [[CBRCloudKitConnection alloc] initWithDatabase:database];
    
    _cloudBridge = [[CBROfflineCapableCloudBridge alloc] initWithCloudConnection:connection coreDataStack:stack];
    [NSManagedObject setCloudBridge:_cloudBridge];
    
    NSLog(@"isRunningInOfflineMode: %d", _cloudBridge.isRunningInOfflineMode);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contextDidChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:[CloudKitCoreDataStack sharedInstance].mainThreadManagedObjectContext];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineStatusChanged:) name:FXReachabilityStatusDidChangeNotification object:nil];
    
//    self.managedObjectContext = [CoreDataHelper sharedHelper].context;
//    [self.managedObjectContext setUndoManager:nil];
    // pass down our managedObjectContext to our RecipeListTableViewController
//    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
//    UINavigationController *navController = tabBarController.viewControllers[0];
    
//    RecipeListTableViewController *recipeListVC = (RecipeListTableViewController *)navController.topViewController;
//    recipeListVC.managedObjectContext = [CloudKitCoreDataStack sharedInstance].mainThreadManagedObjectContext;
    
//    // Allocate a reachability object
//    Reachability* reachab = [Reachability reachabilityWithHostname:@"www.google.com"];
//    
//    // Set the blocks
//    reachab.reachableBlock = ^(Reachability *reach)
//    {
//        // keep in mind this is called on a background thread
//        // and if you are updating the UI it needs to happen
//        // on the main thread, like this:
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"REACHABLE!");
            [_cloudBridge reenableOnlineModeWithCompletionHandler:^(NSError *error) {
                if (error) {
                    NSLog(@"%@", error.localizedDescription);
                }
                NSLog(@"isRunningInOfflineMode: %d", _cloudBridge.isRunningInOfflineMode);
            }];
//        });
//    };
//    
//    reachab.unreachableBlock = ^(Reachability *reach)
//    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"NOT REACHABLE!");
//            [_cloudBridge enableOfflineMode];
//        });
//    };
//    
//    // Start the notifier, which will cause the reachability object to retain itself!
//    [reachab startNotifier];
    
    return YES;
}
- (void)sync{
//    [Recipe fetchObjectsMatchingPredicate:<#(NSPredicate *)#> withCompletionHandler:<#^(NSArray *fetchedObjects, NSError *error)completionHandler#>]
}
//- (void)onlineStatusChanged:(NSNotification *)note{
//    NSLog(@"%s",__PRETTY_FUNCTION__);
//    BOOL reachable = [FXReachability isReachable];
//    NSLog(@"reachable %d", reachable);
//    if (!reachable) {
//        [_cloudBridge enableOfflineMode];
//    }else {
//        [_cloudBridge reenableOnlineModeWithCompletionHandler:^(NSError *error) {
//            if (error) {
//                NSLog(error.localizedDescription);
//            }
//        }];
//    }
//}
// saves changes in the application's managed object context before the application terminates.
//
- (void)applicationWillTerminate:(UIApplication *)application {
	
//    NSError *error;
//    if (self.managedObjectContext != nil) {
//        if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error]) {
//			/*
//			 Replace this implementation with code to handle the error appropriately.
//			 
//			 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
//			 */
//			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//			abort();
//        } 
//    }
}


//#pragma mark - Core Data stack
//
///**
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
// */
//- (NSManagedObjectContext *)managedObjectContext {
//	
//    if (_managedObjectContext != nil) {
//        return _managedObjectContext;
//    }
//	
//    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//    if (coordinator != nil) {
//        _managedObjectContext = [NSManagedObjectContext new];
//        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
//    }
//    return _managedObjectContext;
//}
//
///**
// Returns the managed object model for the application.
// If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
// */
//- (NSManagedObjectModel *)managedObjectModel {
//	
//    if (_managedObjectModel != nil) {
//        return _managedObjectModel;
//    }
//    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
//    return _managedObjectModel;
//}
//
///**
// Returns the URL to the application's documents directory.
// */
//- (NSURL *)applicationDocumentsDirectory
//{
//    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//}
//
///**
// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
// */
//- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
//	
//    if (_persistentStoreCoordinator != nil) {
//        return _persistentStoreCoordinator;
//    }
//		
//	// copy the default store (with a pre-populated data) into our Documents folder
//    //
//    NSString *documentsStorePath =
//        [[[self applicationDocumentsDirectory] path] stringByAppendingPathComponent:@"Recipes.sqlite"];
//	
//    // if the expected store doesn't exist, copy the default store
//	if (![[NSFileManager defaultManager] fileExistsAtPath:documentsStorePath]) {
//		NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:@"Recipes" ofType:@"sqlite"];
//		if (defaultStorePath) {
//			[[NSFileManager defaultManager] copyItemAtPath:defaultStorePath toPath:documentsStorePath error:NULL];
//		}
//	}
//
//    _persistentStoreCoordinator =
//        [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//    
//    // add the default store to our coordinator
//    NSError *error;
//    NSURL *defaultStoreURL = [NSURL fileURLWithPath:documentsStorePath];
//   if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
//                                                  configuration:nil
//                                                            URL:defaultStoreURL
//                                                        options:nil
//                                                          error:&error]) {
//		/*
//		 Replace this implementation with code to handle the error appropriately.
//		 
//		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
//		 
//		 Typical reasons for an error here include:
//		 * The persistent store is not accessible
//		 * The schema for the persistent store is incompatible with current managed object model
//		 Check the error message to determine what the actual problem was.
//		 */
//		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//		abort();
//    }
//    
//    // setup and add the user's store to our coordinator
//    NSURL *userStoreURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"UserRecipes.sqlite"];
//    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
//                                                                     configuration:nil
//                                                                               URL:userStoreURL
//                                                                           options:nil
//                                                                             error:&error]) {
//		/*
//		 Replace this implementation with code to handle the error appropriately.
//		 
//		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
//		 
//		 Typical reasons for an error here include:
//		 * The persistent store is not accessible
//		 * The schema for the persistent store is incompatible with current managed object model
//		 Check the error message to determine what the actual problem was.
//		 */
//		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//		abort();
//    }
//		
//    return _persistentStoreCoordinator;
//}
- (void)contextDidChange:(NSNotification *)notification
{
    //    if ([self isViewLoaded]) {
    //		[self updateLocations];
    //	}
    //    dispatch_async(dispatch_get_main_queue(), ^{
    assert([NSManagedObjectContextObjectsDidChangeNotification isEqual:notification.name]);
    for (NSManagedObject *mo in notification.userInfo[NSUpdatedObjectsKey])
    {
        NSLog(@"Updated: %@",mo.description);
//        if ([mo isKindOfClass:[ContactAddress class]]) {
//            ContactAddress *address = (ContactAddress *)mo;
//            MKAnnotationView *paV = [_mapView viewForAnnotation:address];
//            //            if (crime.type.length > 0) {
//            //                paV.image = [UIImage imageNamed:crime.type];
//            //            }else paV.image = [UIImage imageNamed:@"Crime"];
//        }
    }
    for (NSManagedObject *mo in notification.userInfo[NSInsertedObjectsKey])
    {
        NSLog(@"Created: %@",mo.description);
//        if ([mo isKindOfClass:[ContactAddress class]]) {
//            NSLog(@"ContactAddress ADDED TO MAP");
//            ContactAddress *address;
//            address = (ContactAddress *)mo;
//            
//            if (address.coordinate.latitude > 0 && address.coordinate.longitude > 0) {
//                [_mapView addAnnotation:address];
//            }else{
//                [self geocodeContactAddress:address];
//            }
//        }
    }
    for (NSManagedObject *mo in notification.userInfo[NSDeletedObjectsKey])
    {
        NSLog(@"Deleted: %@",mo.description);
//        if ([mo isKindOfClass:[ContactAddress class]]) {
//            ContactAddress *address = nil;
//            address = (ContactAddress *)mo;
//            [_mapView removeAnnotation:(ContactAddress *)mo];
//        }
    }
}
@end
