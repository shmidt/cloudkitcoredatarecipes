/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract: View controller to manage a text view to allow the user to edit instructions for a recipe.
 
  Version: 1.5
*/

@class Recipe;

@interface InstructionsViewController : UIViewController

@property (nonatomic, strong) Recipe *recipe;

@end
