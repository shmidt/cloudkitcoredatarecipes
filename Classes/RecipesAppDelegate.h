/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract: Application delegate that sets up a tab bar controller with two view controllers -- a navigation controller that in turn loads a table view controller to manage a list of recipes, and a unit converter view controller.
 
  Version: 1.5
*/

@interface RecipesAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

