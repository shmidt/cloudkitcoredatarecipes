/*
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract: Table view controller to manage editing details of a recipe ingredient -- its name and amount.
 
  Version: 1.5
*/

#import "IngredientDetailViewController.h"
#import "Recipe.h"
#import "Ingredient.h"
#import "EditingTableViewCell.h"

@interface IngredientDetailViewController ()

// table's data source
@property (nonatomic, strong) NSString *ingredientStr;
@property (nonatomic, strong) NSString *amountStr;

@end

// view tags for each UITextField
#define kIngredientFieldTag     1
#define kAmountFieldTag         2

static NSString *IngredientsCellIdentifier = @"IngredientsCell";


@implementation IngredientDetailViewController

- (void)viewDidLoad {
    
	[super viewDidLoad];
    
	self.title = @"Ingredient";
    
    self.tableView.allowsSelection = NO;
	self.tableView.allowsSelectionDuringEditing = NO;
}

- (void)setIngredient:(Ingredient *)ingredient {
    
    _ingredient = ingredient;
    
    _ingredientStr = ingredient.name;
    _amountStr = ingredient.amount;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EditingTableViewCell *cell =
        (EditingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:IngredientsCellIdentifier
                                                                forIndexPath:indexPath];
    if (indexPath.row == 0) {
        // cell ingredient name
        cell.label.text = @"Ingredient";
        cell.textField.text = self.ingredientStr;
        cell.textField.placeholder = @"Name";
        cell.textField.tag = kIngredientFieldTag;
    }
	else if (indexPath.row == 1) {
        // cell ingredient amount
        cell.label.text = @"Amount";
        cell.textField.text = self.amountStr;
        cell.textField.placeholder = @"Amount";
        cell.textField.tag = kAmountFieldTag;
    }

    return cell;
}


#pragma mark - Save and cancel

- (IBAction)save:(id)sender {
    NSLog(@"%s",__PRETTY_FUNCTION__);
	// if there isn't an ingredient object, create and configure one
    if (!self.ingredient) {
        self.ingredient = [NSEntityDescription insertNewObjectForEntityForName:@"Ingredient"
                                                          inManagedObjectContext:self.recipe.managedObjectContext];
        [self.ingredient createWithCompletionHandler:^(id managedObject, NSError *error) {
            NSLog(@"Created ingredient: %@", managedObject);
            if (error) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }];
        self.ingredient.recipe = self.recipe;
        self.ingredient.displayOrder = [NSNumber numberWithInteger:self.recipe.ingredients.count];
    }
	
	// update the ingredient from the values in the text fields
    EditingTableViewCell *cell;
	
    cell = (EditingTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    self.ingredient.name = cell.textField.text;
	
    cell = (EditingTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    self.ingredient.amount = cell.textField.text;
	
    [self.ingredient saveWithCompletionHandler:^(id managedObject, NSError *error) {
        NSLog(@"Updating ingredient: %@", managedObject);
        if (error) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }];
	
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    // editing has ended in one of our text fields, assign it's text to the right
    // ivar based on the view tag
    //
    switch (textField.tag)
    {
        case kIngredientFieldTag:
            self.ingredientStr = textField.text;
            break;
            
        case kAmountFieldTag:
            self.amountStr = textField.text;
            break;
    }
}

@end
