//
//  AbstractCloudRecord.h
//  Recipes
//
//  Created by Dmitry Shmidt on 20/09/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AbstractCloudRecord : NSManagedObject<CBRCloudKitEntity, CBROfflineCapableManagedObject>

@property (nonatomic, retain) NSString * recordIDString;
@property (nonatomic, retain) NSNumber *hasPendingCloudBridgeChanges;
@property (nonatomic, retain) NSNumber *hasPendingCloudBridgeDeletion;
@end
