
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CBRCloudKitConnection
#define COCOAPODS_POD_AVAILABLE_CBRCloudKitConnection
#define COCOAPODS_VERSION_MAJOR_CBRCloudKitConnection 0
#define COCOAPODS_VERSION_MINOR_CBRCloudKitConnection 9
#define COCOAPODS_VERSION_PATCH_CBRCloudKitConnection 3

// CBRManagedObjectCache
#define COCOAPODS_POD_AVAILABLE_CBRManagedObjectCache
#define COCOAPODS_VERSION_MAJOR_CBRManagedObjectCache 1
#define COCOAPODS_VERSION_MINOR_CBRManagedObjectCache 3
#define COCOAPODS_VERSION_PATCH_CBRManagedObjectCache 0

// CloudBridge
#define COCOAPODS_POD_AVAILABLE_CloudBridge
#define COCOAPODS_VERSION_MAJOR_CloudBridge 0
#define COCOAPODS_VERSION_MINOR_CloudBridge 9
#define COCOAPODS_VERSION_PATCH_CloudBridge 6

// FXReachability
#define COCOAPODS_POD_AVAILABLE_FXReachability
#define COCOAPODS_VERSION_MAJOR_FXReachability 1
#define COCOAPODS_VERSION_MINOR_FXReachability 1
#define COCOAPODS_VERSION_PATCH_FXReachability 1

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// SLCoreDataStack
#define COCOAPODS_POD_AVAILABLE_SLCoreDataStack
#define COCOAPODS_VERSION_MAJOR_SLCoreDataStack 0
#define COCOAPODS_VERSION_MINOR_SLCoreDataStack 8
#define COCOAPODS_VERSION_PATCH_SLCoreDataStack 0

