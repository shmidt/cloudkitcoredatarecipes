//
//  RecipeType.m
//  Recipes
//
//  Created by Dmitry Shmidt on 26/09/14.
//
//

#import "RecipeType.h"
#import "Recipe.h"


@implementation RecipeType

@dynamic name;
@dynamic recipes;

@end
