//
//  AbstractCloudRecord.m
//  Recipes
//
//  Created by Dmitry Shmidt on 20/09/14.
//
//

#import "AbstractCloudRecord.h"


@implementation AbstractCloudRecord

@dynamic recordIDString;
@dynamic hasPendingCloudBridgeChanges, hasPendingCloudBridgeDeletion;
-(void)awakeFromInsert {
    NSLog(@"%s",__PRETTY_FUNCTION__);
//    if (![self primitiveValueForKey:@"wid"]) {
//        NSLog(@"Nil values here...");
//    }
    self.recordIDString = [NSUUID UUID].UUIDString;
    [super awakeFromInsert];
}
@end
