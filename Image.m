//
//  Image.m
//  Recipes
//
//  Created by Dmitry Shmidt on 26/09/14.
//
//

#import "Image.h"
#import "Recipe.h"


@implementation Image

@dynamic image;
@dynamic recipe;

@end
